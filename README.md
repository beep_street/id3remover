#### Name:
ID3remover - Detect and remove ID3 tags from FLAC files.

#### Synopsis:
`ID3remover.sh [ --help | --clean | --every | --force | --test ] FLAC_FILE [FLAC_FILE]...`

#### Description:
Does not modify source files. Exports vorbis comments and first PICTURE block, decodes to wav, encodes a new flac file, verifies matching audio data, imports exported metadata, and cleans up temp files.

> "Just remember ID3 was the first! =), even though it did suck!" -Eric Kemp, originator of ID3 1.0

#### Depends:
`basename, dd, dirname, flac, metaflac, mkdir, rm, wc`

#### Documentation:
`-h | -H | --help` Print full help text.
