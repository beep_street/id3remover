#!/bin/sh
# shellcheck disable=SC2086 # we WANT zero arguments when $force is empty

# ID3remover.sh: detect and remove ID3 tags in FLAC files, using only POSIX shell features
# description: Does not modify source files. Exports vorbis comments and first PICTURE block, decodes to wav, encodes
#              a new flac file, verifies matching audio data, imports exported metadata, and cleans up temp files.
#              "Just remember ID3 was the first! =), even though it did suck!" -Eric Kemp, originator of ID3 1.0
# version: 0.1
# depends: basename, dd, dirname, flac, metaflac, mkdir, rm, wc
# usage: $ ID3remover.sh [ --help | --clean | --every | --force | --test ] FLAC_FILE [FLAC_FILE]...

padding="4096" # bytes of padding to use in output flac files
out_dir_name="output" # name to use for output folder
success_message="This was a triumph." # if set with "0", success message is disabled, script will only emit detected ID3 flacs and errors
id3_everywhere="" # if set with "1" (and not running with --clean, --force, or --test), wantonly treat EVERY FLAC FILE found as if it has ID3 tags

_clean_temp_files() {
	[ -e "$id3_flac_tags" ] && rm "$id3_flac_tags"
	[ -e "$id3_flac_art" ]  && rm "$id3_flac_art"
	[ -e "$id3_flac_wav" ]  && rm "$id3_flac_wav"
}

_help() {
	echo "Usage:"
	printf '      %s [ -h | -c | -e | -f | -t ] FLAC_FILE [FLAC_FILE]...\n' "$0"
	echo
	echo "Options (are mutually exclusive):"
	echo
	echo "-h, -H, --help    Print this help text."
	echo
	echo "-c, --clean       Remove any existing temporary files associated with FLAC files in script arguments."
	echo "-e, --every       Override tests, treat every FLAC file found as if it has ID3 tags."
	echo "-f, --force       Use if flac prints an error about existing input/output files and you want to override."
	echo "-t, --test        Test for ID3 tags without removing them, lists all ID3-having FLACs found in arguments."
	echo
}

_remove_id3_tags() { # does almost all the stuff
	unset embed_art_export ffp_source ffp_output metaflac_import # no "local" / "declare" in POSIX

	if metaflac --export-tags-to="$id3_flac_tags" "$id3_flac" ;then # export existing Vorbis tags to file
		metaflac --export-picture-to="$id3_flac_art" "$id3_flac" >/dev/null 2>&1 && embed_art_export="0" # because metaflac, exporting art may be the best test for it

		ffp_source="$( metaflac --show-md5sum "$id3_flac" )" # store source's fingerprint
		if flac --silent $force --decode --output-prefix="$id3_flac_dir"/ "$id3_flac" ;then # decode source flac to wav, saving in same dir

			[ ! -d "$out_dir" ] && mkdir -p "$out_dir" # check for output dir, create it if needed
			if flac --silent $force -V8 --no-padding --output-prefix="$out_dir"/ "$id3_flac_wav" ;then # re-encode wav back to flac

				ffp_output="$( metaflac --show-md5sum "$out_flac" )" # store output's fingerprint
				if [ "$ffp_source" = "$ffp_output" ] ;then # verify identical audio (for the 3rd time(?) - flac does this already on every decode/encode)

					if [ "$embed_art_export" = "0" ] ;then # import saved Vorbis comments to new ID3-free FLAC file, import embedded artwork, add padding
						metaflac --import-tags-from="$id3_flac_tags" --import-picture-from="$id3_flac_art" --add-padding="$padding" "$out_flac" && metaflac_import="0"
					else
						metaflac --import-tags-from="$id3_flac_tags"                                       --add-padding="$padding" "$out_flac" && metaflac_import="0"
					fi

					if [ "$metaflac_import" = "0" ] ;then
						[ "$success_message" != "0" ] && printf '%s\n' "$success_message"
					else
						echo "Importing saved tags failed!"
						return 1
					fi
				else
					echo "Decoding / re-encoding failed: FLAC fingerprints do not match!"
					return 1
				fi
			else
				echo "FLAC re-encoding failed!"
				return 1
			fi
		else
			echo "FLAC decoding failed!"
			return 1
		fi
	else
		echo "Exporting tags failed!"
		return 1
	fi
	return 0
}

_test_id3_tags() { # v2 - returns 0 when ID3 tags in (hopefully) any version or possibly-predicable position are detected
	# http://fileformats.archiveteam.org/wiki/ID3
	# id3v1: "TAG" appears 128 bytes from end of file
	# id3v2: "ID3" appears at beginning of file, or "3DI" could appear 10 bytes from end of file, or "3DI" could appear 138 bytes from end of file
	[ "$id3_everywhere" = "1" ] && return 0

	unset test_flac length id3v1 id3v2_0 id3v2_1 id3v2_2

	test_flac="$1"
	length="$( wc -c "$test_flac" )" # https://hydrogenaud.io/index.php?topic=97701.msg813074#msg813074
	length="${length%% *}"
	
	id3v1="$( dd if="$test_flac" bs=1 skip="$(( length - 128 ))" count=3 status=none 2>/dev/null )" ||
		{ printf 'dd had non-zero exit status for id3v1 test on %s, cannot confirm ID3 tags.\n' "$test_flac" ;return 1 ; }
	[ "$id3v1" = "TAG" ] && return 0
	
	id3v2_0="$( dd if="$test_flac" bs=1 skip=0 count=3 status=none 2>/dev/null )" ||
		{ printf 'dd had non-zero exit status for id3v2_0 test on %s, cannot confirm ID3 tags.\n' "$test_flac" ;return 1 ; }
	[ "$id3v2_0" = "ID3" ] && return 0
	
	id3v2_1="$( dd if="$test_flac" bs=1 skip="$(( length - 10 ))" count=3 status=none 2>/dev/null )" ||
		{ printf 'dd had non-zero exit status for id3v2_1 test on %s, cannot confirm ID3 tags.\n' "$test_flac" ;return 1 ; }
	[ "$id3v2_1" = "3DI" ] && return 0
	
	id3v2_2="$( dd if="$test_flac" bs=1 skip="$(( length - 138 ))" count=3 status=none 2>/dev/null )" ||
		{ printf 'dd had non-zero exit status for id3v2_2 test on %s, cannot confirm ID3 tags.\n' "$test_flac" ;return 1 ; }
	[ "$id3v2_2" = "3DI" ] && return 0

	return 1
}

while true ;do # say when
	case $1 in # run all commands, until ';;', under the first pattern|match) in the list equaling $1
		-h|-H|--help)
			_help ;exit 0
			;;
		-c|--clean)
			clean_temp_files="1"
			unset id3_everywhere force test_only # one option at a time please
			shift # $2 is now $1, etc
			;;
		-e|--every)
			id3_everywhere="1"
			unset clean_temp_files force test_only
			shift
			;;
		-f|--force)
			force="--force"
			unset clean_temp_files id3_everywhere test_only
			shift
			;;
		-t|--test)
			test_only="1"
			unset clean_temp_files id3_everywhere force
			shift
			;;
		--) # end of options
			shift ;break # when
			;;
		-?*) # bad options
			printf 'Unknown option, %s, aborting. Maybe try using --.\n' "$1" ;exit 1
			;;
		*)  # none of the above, it's probably a file
			break # when
			;;
	esac
done

[ "$#" -lt "1" ] && { echo "No arguments. Nothing to do." ;exit 1 ; }

for arg in "$@" ;do
	case $arg in
		*.[Ff][Ll][Aa][Cc]) # first input/argument test
			if [ -e "$1" ] ;then # possible user input includes *.flac -matching strings which are not valid paths to flac files
				flacs_found="1"  # make a note for later that we did indeed find at least one file that does exist, and has a .flac extension
			else
				continue # start for loop over with the next arg
			fi
			;;
		*) # any other pattern
			continue
			;;
	esac

	id3_flac="$arg"
	id3_flac_dir="$( dirname "$id3_flac" ;echo x )" # relative dirname of flac file, with "safety character", as per https://www.etalabs.net/sh_tricks.html
	id3_flac_dir="${id3_flac_dir%??}" # strip safety charater and dirname's added newline to preserve any pre-existing newlines
	id3_flac_ext="${id3_flac##*.}" # candidate flac's extension (thank you again "Rich's sh tricks" - parameter expansion _is_ POSIX, I am so oblivious)
	id3_flac_base="$( basename "$id3_flac" ."$id3_flac_ext" ;echo x )" # name of candidate flac file, without extension, with safety character
	id3_flac_base="${id3_flac_base%??}" # strip safety character, etc
	id3_flac_tags="${id3_flac}.metaflac"
	id3_flac_art="${id3_flac}.embed.img"
	id3_flac_wav="${id3_flac_dir}/${id3_flac_base}.wav"
	out_dir="${id3_flac_dir}/${out_dir_name}" # output directory, subdir of candidate flac file's parent
	out_flac="${out_dir}/${id3_flac_base}.flac" # fixed/output flac file

	if [ "$clean_temp_files" = "1" ] ;then
		_clean_temp_files

	elif [ "$test_only" = "1" ] ;then
		_test_id3_tags "$arg" && { printf 'ID3 tags found: %s\n' "$arg" ;id3_tags_found="1" ; }

	else
		if _test_id3_tags "$arg" ;then       # if the testing function detects ID3 tags...
			id3_tags_found="1"
			printf 'Removing ID3 tags from: %s\n' "$arg"
			if _remove_id3_tags "$arg" ;then # remove them
				_clean_temp_files            # and remove temporary .embed.img, .metaflac, and .wav files
				# insert your own source-file-deleting code here perhaps, eg:
				#[ "$deletion_desire_certainty" = "utterly_certain" ] && { rm "$id3_flac" ;echo "and ...?" ; }
			fi
			echo
		fi
	fi

	unset id3_flac id3_flac_dir id3_flac_ext id3_flac_base id3_flac_tags id3_flac_art id3_flac_wav out_dir out_flac
done

if [ "$flacs_found" = "1" ] ;then # covered all bases here? --clean provides no output unless no FLACs match args... (which is ... just fine..?)
	[ "$clean_temp_files" != "1" ] && [ "$id3_tags_found" != "1" ] && echo "No ID3 tags found on provided FLAC files."
else
	echo "No FLAC files found."
fi
